/*
Originally made by Ryan Carrusca

Purpose: to help with chemistry calculations

*/
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#define AVOGADRO 6.022*pow(10,23)
#define STP_VOL 22.4

using namespace std;

string slice(string, int, int);
bool isNumber(char);
bool isCap(char);
double molarMass(string, const map<string, double> &weightsD);


int main() {
	int elemNum;
	int mode;
	string input;
	double input2;
	string input3;
	string input4;
	double input5;
	double output;
	enum modes { quit, conversion, molar_mass, composition};


	ifstream file("weights.txt", ios::in);

	file >> elemNum;

	vector <string> names(elemNum);
	vector <string> symbols(elemNum);
	vector <int> numbers(elemNum);
	vector <double> weights(elemNum);
	map<string, double> weightsD;

	for (int i = 0; i < elemNum; i++) {
		file >> names[i] >> symbols[i] >> numbers[i] >> weights[i];
		weightsD[symbols[i]] = weights[i];
	}
	file.close();

	cout << "Modes:" << endl <<
		quit << " - quit" << endl <<
		conversion << " - conversion" << endl <<
		molar_mass << " - molar mass calculator" << endl <<
		composition << " - % composition of an element in a compound by mass" << endl;

	while (1) {
		cout << "Mode: ";
		cin >> mode;

		if (mode == quit) {
			return 0;
		}
		else if (mode == conversion) {
			cout << "Convert from one measurement to another. Enter compound, initial measurement, initial value, and what you want to convert to." << endl
				<< "Options are mass (g), moles, molecules, volume (L, at STP). ";
			cin >> input >> input3 >> input2 >> input4;

			if (input3 == "mass") {
				output = input2 / molarMass(input, weightsD);
			}
			else if (input3 == "moles") {
				output = input2;
			}
			else if (input3 == "molecules") {
				output = input2 / AVOGADRO;
			}
			else if (input3 == "volume") {
				output = input2 / STP_VOL;
			}

			if (input4 == "mass") {
				output = output * molarMass(input, weightsD);
			}
			else if (input4 == "molecules") {
				output = output * AVOGADRO;
			}
			else if (input4 == "volume") {
				output = output * STP_VOL;
			}

			cout << input2 << " " << input3 << " of " << input << " equals " << output << " " << input4 << endl;
		}
		else if (mode == molar_mass) {
			cout << "Enter compound to find the molar mass: ";
			cin >> input;
			cout << "Molar mass of " << input << " is " << molarMass(input, weightsD) << "g/mol" << endl;
		}
		else if (mode == composition) {
			cout << "Enter element, moles of element, and total mass (g): ";
			cin >> input >> input2 >> input5;
			output = input2 * molarMass(input, weightsD) / input5 * 100;
			cout << "The " << input5 << "g unknown substance is " << output << "% " << input << endl;

		}
		else {
			cout << "Unknown mode" << endl;
			continue;
		}
		system("PAUSE");
		mode = 0;
	}

	return 0;
}


string slice(string input, int begin, int end) {
	/* returns slice of string from index begin to index end inclusive*/
	string output="";
	for (int i = begin; i <= end; i++) {
		output += input[i];
	}
	return output;
}


bool isNumber(char inp) {
	string digits = "0123456789";
	for (int i = 0; i < 11; i++) {
		if (inp == digits[i]) {
			return true;
		}
	}
	return false;
}


bool isCap(char inp) {
	/* returns true if the input is a capital letter found in periodic table symbols, otherwise false */
	string caps = "CPASNTBRHUFLMEGIKOYZDWVX"; //in order from most to least common capital letters in atomic symbols
	for (int i = 0; i < 25; i++) {
		if (inp == caps[i]) {
			return true;
		}
	}
	return false;
}


double molarMass(string input, const map<string, double> &weightsD) {
	unsigned character, j;
	string symbolTemp = "";
	string coefficientTemp = "";
	double mass = 0.0;
	vector <char> vInput;

	for (int k = 0; input[k] != '\0'; k++) {
		vInput.push_back(input[k]);
	}

	for (character = 0; character < vInput.size(); character++) {
		if (vInput[character] == ')') {
			if (character) {
				if (coefficientTemp == "") {
					coefficientTemp = "1";
				}
				mass += stoi(coefficientTemp)* weightsD.at(symbolTemp);
			}
			return mass;
		}
		else if (vInput[character] == '(') { //for multiple polyatomic ions
			if (character) {
				if (coefficientTemp == "") {
					coefficientTemp = "1";
				}
				mass += stoi(coefficientTemp)* weightsD.at(symbolTemp);
				coefficientTemp = "";
				symbolTemp = "";
			}
			for (j = character; j < vInput.size(); j++){
				if (vInput[j] == ')') {
					mass += atoi(&vInput[j + 1]) * molarMass(slice(input, character + 1, j), weightsD);
					character = j+1;
					break;
				}

			}
			
		}
		else if (isCap(vInput[character])) {
			if (character) {
				if (coefficientTemp == "") {
					coefficientTemp = "1";
				}
				mass += stoi(coefficientTemp)* weightsD.at(symbolTemp);
				coefficientTemp = "";
				symbolTemp = "";
			}
			symbolTemp += vInput[character];
		}

		else if (isNumber(vInput[character])) {
			coefficientTemp += vInput[character];
		}

		else {
			symbolTemp += vInput[character];
		}

	}
	if (symbolTemp != "") {
		if (coefficientTemp == "") {
			coefficientTemp = "1";
		}
		mass += stoi(coefficientTemp)*weightsD.at(symbolTemp);
	}
	
	return mass;
}